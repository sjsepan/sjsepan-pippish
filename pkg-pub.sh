#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./sjsepan-pippish-0.1.10.vsix
npx ovsx publish sjsepan-pippish-0.1.10.vsix --debug --pat <PAT>
