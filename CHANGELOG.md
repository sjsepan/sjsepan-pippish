# Pippish Color Theme - Change Log

## [0.1.10]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.9]

- update readme and screenshot

## [0.1.8]

- fix pub issues on OVSX

## [0.1.2]

- testing publish

## [0.1.1]

- remove ./ from rel paths

## [0.1.0]

- dimmer buttons BG so as not to compete visually with project names in Git sidebar
- fix manifest repo links
- retain v0.0.2 for those that prefer earlier style

## [0.0.2]

- reverse button.FG/BG

## [0.0.1]

- Initial release
